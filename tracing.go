package tracing

import (
	"io"
	"net/http"
	"net/url"

	ot "github.com/opentracing/opentracing-go"
	ext "github.com/opentracing/opentracing-go/ext"
)

const (
	clientDo = iota + 1
	clientGet
	clientHead
	clientPost
	clientPostFrom
)

type request struct {
	URL         string
	req         *http.Request
	contentType string
	body        io.Reader
	data        url.Values
}

//Handler for integration with net/http.
func Handler(h http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wireContext, _ := ot.GlobalTracer().Extract(ot.HTTPHeaders, ot.HTTPHeadersCarrier(r.Header))
		span := ot.GlobalTracer().StartSpan(name, ext.RPCServerOption(wireContext))
		span.SetTag(string(ext.SpanKind), string(ext.SpanKindRPCServerEnum))
		span.SetTag(string(ext.PeerHostname), r.Host)
		span.SetTag(string(ext.HTTPUrl), r.URL.Path)
		span.SetTag(string(ext.HTTPMethod), r.Method)
		h.ServeHTTP(w, r)
		span.SetTag(string(ext.HTTPStatusCode), "200")
		ot.GlobalTracer().Inject(span.Context(), ot.HTTPHeaders, ot.HTTPHeadersCarrier(w.Header()))
		span.Finish()
	})
}

// HandlerFuncWithNext for integration with github.com/urfave/negroni.
func HandlerFuncWithNext(w http.ResponseWriter, r *http.Request, next http.HandlerFunc, name string) {
	wireContext, _ := ot.GlobalTracer().Extract(ot.HTTPHeaders, ot.HTTPHeadersCarrier(r.Header))
	span := ot.GlobalTracer().StartSpan(name, ext.RPCServerOption(wireContext))
	span.SetTag(string(ext.SpanKind), string(ext.SpanKindRPCServerEnum))
	span.SetTag(string(ext.PeerHostname), r.Host)
	span.SetTag(string(ext.HTTPUrl), r.URL.Path)
	span.SetTag(string(ext.HTTPMethod), r.Method)
	next(w, r)
	span.SetTag(string(ext.HTTPStatusCode), "200")
	ot.GlobalTracer().Inject(span.Context(), ot.HTTPHeaders, ot.HTTPHeadersCarrier(w.Header()))
	span.Finish()
}

// Get performs a traced http.Client.Get()
func Get(c *http.Client, url string) (*http.Response, error) {
	return tracedRequest(c, request{
		URL: url,
	}, clientGet)
}

// Do performs a traced http.Client.Do()
func Do(c *http.Client, req *http.Request) (*http.Response, error) {
	return tracedRequest(c, request{
		req: req,
	}, clientDo)
}

// Head performs a traced http.Client.Head()
func Head(c *http.Client, url string) (*http.Response, error) {
	return tracedRequest(c, request{
		URL: url,
	}, clientHead)
}

// Post performs a traced http.Client.Post()
func Post(c *http.Client, url string, contentType string, body io.Reader) (resp *http.Response, err error) {
	return tracedRequest(c, request{
		URL:         url,
		contentType: contentType,
		body:        body,
	}, clientPost)
}

// PostForm performs a traced http.Client.PostForm()
func PostForm(c *http.Client, url string, data url.Values) (resp *http.Response, err error) {
	return tracedRequest(c, request{
		URL:  url,
		data: data,
	}, clientPostFrom)
}

func tracedRequest(c *http.Client, r request, fn int) (*http.Response, error) {
	span := ot.StartSpan("http-request")
	span.SetTag(string(ext.SpanKind), string(ext.SpanKindRPCClientEnum))
	if len(r.URL) > 0 {
		span.SetTag(string(ext.HTTPUrl), r.URL)
	} else {
		span.SetTag(string(ext.HTTPUrl), r.req.URL.String())
	}
	var e error
	var resp *http.Response
	switch fn {
	case clientDo:
		resp, e = c.Do(r.req)
	case clientGet:
		resp, e = c.Get(r.URL)
	case clientHead:
		resp, e = c.Head(r.URL)
	case clientPost:
		resp, e = c.Post(r.URL, r.contentType, r.body)
	case clientPostFrom:
		resp, e = c.PostForm(r.URL, r.data)
	}
	span.SetTag(string(ext.HTTPMethod), resp.Request.Method)
	span.SetTag(string(ext.HTTPStatusCode), resp.StatusCode)
	span.Finish()
	return resp, e
}
