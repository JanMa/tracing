# Tracing

A simple HTTP middleware for Go to trace outgoing and incoming requests.
It provides handler functions for use with "net/http" or "github.com/urfave/negroni"
and also wrappers around http.Client's functions to perfrom outgoing traced requests.
